<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>PrefWindowWidget</name>
    <message>
        <location filename="../PrefWindowWidget.ui" line="46"/>
        <source>CloudShutter Preferences</source>
        <translation>Настройки CloudShutter</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="74"/>
        <source>General</source>
        <translation>Общие</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="86"/>
        <source>Exit Confirmation</source>
        <translation>Подтверждать выход</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="115"/>
        <source>Run at startup</source>
        <translation>Запускать при старте</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="128"/>
        <source>Language:</source>
        <translation>Язык:</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="134"/>
        <source>Hot keys</source>
        <translation>Горячие клавиши</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="139"/>
        <source>Storage</source>
        <translation>Хранилища</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="144"/>
        <source>Credits</source>
        <translation>Участие</translation>
    </message>
</context>
<context>
    <name>SysTrayApp</name>
    <message>
        <location filename="../systrayapp.cpp" line="80"/>
        <source>Ask me</source>
        <translation>Запрашивать</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="76"/>
        <source>Screenshot</source>
        <translation>Снимок экрана</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="77"/>
        <source>Aim Frame</source>
        <translation>Выбрать кадр</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="78"/>
        <source>Select Window</source>
        <translation>Выбрать окно</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="79"/>
        <source>Storage options</source>
        <translation>Варианты хранилищ</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="81"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="82"/>
        <source>Clipboard</source>
        <translation>Буфер обмена</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="83"/>
        <source>Preferences</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="84"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="56"/>
        <source>Do you really want to quit CloudShutter?</source>
        <translation>Правда хотите выйти из CloudShutter?</translation>
    </message>
</context>
</TS>
