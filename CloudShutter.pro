#-------------------------------------------------
#
# Project created by QtCreator 2014-04-16T16:28:46
#
#-------------------------------------------------

# DEFINES += QT_NO_CAST_FROM_ASCII - not clear if thi is realy needed

QT       += core gui
QT       += svg widgets

TARGET = CloudShutter
TEMPLATE = app

SOURCES += main.cpp\
    systrayapp.cpp \
    PrefWindowWidget.cpp \
    semaphorelistener.cpp

HEADERS  +=        \
            systrayapp.h \
    PrefWindowWidget.h \
    semaphorelistener.h

FORMS    += \
    PrefWindowWidget.ui

RESOURCES += \
    res/cs_resources.qrc \
    languages/translations.qrc


lupdate_only{
SOURCES = *.qml \
          *.js  \
          *.cpp \
          *.ui
}

TRANSLATIONS = languages/CloudShutter_en.ts \
               languages/CloudShutter_ru.ts \
               languages/CloudShutter_ro.ts

CONFIG += c++11
