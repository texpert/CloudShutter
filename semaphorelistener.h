#ifndef SEMAPHORELISTENER_H
#define SEMAPHORELISTENER_H

#include <QObject>
#include <QSystemSemaphore>

class SemaphoreListener : public QObject
{
    Q_OBJECT

    QSystemSemaphore *SemaphorePtr;

    public:

        explicit SemaphoreListener(QSystemSemaphore *semafor, QObject *parent = 0);

    signals:
        void SemaphoreAccessed();

    public slots:
        void StartListening();
};

#endif // SEMAPHORELISTENER_H
