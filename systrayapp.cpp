#include "systrayapp.h"

SysTrayApp::SysTrayApp(int &argc, char *argv[]):QApplication(argc, argv)
{
//getting system username - platform dependent
#ifdef Q_OS_WIN
    Username = qgetenv("USERNAME");
#else
    Username = qgetenv("USER");
#endif

//Acquiring a semaphore for avoiding race condition when attaching SharedMemory for current user for SingleApp
//Prepending username to both semaphore and SharedMemory - one instance of program is per user only
    SingleAppMemoryAccessSemaphore = new QSystemSemaphore (Username + QLatin1String(".CloudShutter.Memory.Access.semaphore"), 1, QSystemSemaphore::Create);
    SingleAppMemoryAccessSemaphore->acquire();                                   //semaphore to 0 to avoid another process access to QSharedMemory untill we are setting it

//Initializing a SharedMemory object as a SingleApp instance running flag
    SingleAppFlag = new QSharedMemory (Username + QLatin1String(".CloudShutter.instance"));

//In Unix SharedMemory survives after a program crash, so attaching SharedMemory and,
// then, detaching, will destroy surviving phantom SharedMemory
#ifdef Q_OS_UNIX
    SingleAppFlag->attach();
    SingleAppFlag->detach();
#endif

    SingleAppInstanceSemaphore = new QSystemSemaphore (Username + QLatin1String(".CloudShutter.Instance.Access.semaphore"), 0, QSystemSemaphore::Create);

//if, after cleaning possible surviving phantom SharedMemory, it still exists,
// then the first instance of the application is already running
    if (SingleAppFlag->attach()) {
        QTextStream cout(stdout);
        cout << QLatin1String("CloudShutter already running") << endl;

//Releasing the InstanceSemaphore to signal the main program that an attempt of second opening occured, then exiting
        SingleAppInstanceSemaphore->release();
        delete SingleAppMemoryAccessSemaphore;
        delete SingleAppInstanceSemaphore;
        delete SingleAppFlag;
        ::exit(0);
    }
    else {
//Starting the semaphore listener - it will trigger the Preferences windows when another instance of the application will try to start
        SingleAppFlag->create(1);
        SemaphoreListener *InstanceSemaphoreListener = new SemaphoreListener(SingleAppInstanceSemaphore);
        QThread *InstanceSemaphoreListenerThread = new QThread();
        InstanceSemaphoreListener->moveToThread(InstanceSemaphoreListenerThread);
        connect(InstanceSemaphoreListenerThread, &QThread::started, InstanceSemaphoreListener, &SemaphoreListener::StartListening);
        connect(InstanceSemaphoreListenerThread, &QThread::finished, InstanceSemaphoreListener, &QObject::deleteLater);
        connect(InstanceSemaphoreListener, &SemaphoreListener::SemaphoreAccessed , &PreferenceWindow, &PrefWindowWidget::on_SecondInstanceStartTry);
        InstanceSemaphoreListenerThread->start();
    }

//the SharedMemory task is done - release the Semaphore to unblock possible second instance of program trying to start
    SingleAppMemoryAccessSemaphore->release();

    ScreenshotAction = new QAction(this);
    AimFrameAction = new QAction(this);
    SelectWindowAction = new QAction(this);
    AskAction = new QAction(this);
    AskAction->setCheckable(true);
    ToFileAction = new QAction(this);
    ToClipboardAction = new QAction(this);
    ToClipboardAction->setCheckable(true);
    PreferencesWindowAction = new QAction(this);
    QuitAction = new QAction(this);

    if (QSystemTrayIcon::isSystemTrayAvailable())
    {
        SysTray.setIcon(QIcon(QLatin1String(":cloud check.svg")));

        SysTrayMenu.addAction(ScreenshotAction);
        connect(ScreenshotAction, &QAction::triggered, this, &SysTrayApp::ScreenShotTimer);

        SysTrayMenu.addAction(AimFrameAction);
        connect(AimFrameAction, &QAction::triggered, this, &SysTrayApp::ScreenShotTimer);

        SysTrayMenu.addAction(SelectWindowAction);

        SysTrayMenu.addSeparator();
        SysTrayMenu.addMenu(&SysTrayPipesMenu);
            SysTrayPipesMenu.addAction(AskAction);
            SysTrayPipesMenu.addAction(ToFileAction);
            SysTrayPipesMenu.addAction(ToClipboardAction);
        connect(PreferencesWindowAction, &QAction::triggered, &PreferenceWindow, &PrefWindowWidget::show);
        SysTrayMenu.addAction(PreferencesWindowAction);

        connect(QuitAction, &QAction::triggered, this, &SysTrayApp::quit);
        SysTrayMenu.addAction(QuitAction);

        SysTray.setContextMenu(&SysTrayMenu);

        connect(&PreferenceWindow, &PrefWindowWidget::LanguageChanged, this, &SysTrayApp::SysTrayRetranslate);
        SysTrayRetranslate();
        SysTray.show();
    }
};

SysTrayApp::~SysTrayApp(){
}

void SysTrayApp::quit()
{
    //if(PreferenceWindow.Preferences.value(QStringLiteral("Exit_Confirmation"), true).toBool()){             //popup if Exit_Confirmation is set
    if(PreferenceWindow.QueryCheckBoxState(QLatin1String("Exit_Confirmation")) != 0) {
        msgBox.setText(tr("Do you really want to quit CloudShutter?"));
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::No);
        msgBox.setIcon(QMessageBox::Question);
        switch (msgBox.exec()) {
            case QMessageBox::No:
                break;
            case QMessageBox::Yes:
                QApplication::quit();
                break;
            default:
                break;
        }
    }
    else {
        QApplication::quit();                                                                   //else, quit silently
    }
}

void SysTrayApp::SysTrayRetranslate()
{
    ScreenshotAction->setText(QApplication::translate("SysTrayApp", "Screenshot"));
    AimFrameAction->setText(QApplication::translate("SysTrayApp", "Aim Frame"));
    SelectWindowAction->setText(QApplication::translate("SysTrayApp", "Select Window"));
    SysTrayPipesMenu.setTitle(QApplication::translate("SysTrayApp", "Storage options"));
    AskAction->setText(QApplication::translate("SysTrayApp", "Ask me"));
    ToFileAction->setText(QApplication::translate("SysTrayApp", "File"));
    ToClipboardAction->setText(tr("Clipboard"));
    PreferencesWindowAction->setText(tr("Preferences"));
    QuitAction->setText(tr("Quit"));
    SysTrayTitle1 = QApplication::translate("SysTrayApp", "Screenshot to clipboard");
    SysTrayMessage1 = QApplication::translate("SysTrayApp", "Screenshot has been copied from screen to clipboard");
}

void SysTrayApp::ScreenShotTimer()
{
    //qWarning() << "Sender = " << QObject::sender();
    //qWarning() << "Action = " << ScreenshotAction;
    if (QObject::sender() == ScreenshotAction) {
        QTimer::singleShot(qRound(PreferenceWindow.Preferences.value
                (QLatin1String("Capture_Delay"), 0.1).toDouble()*1000), this, SLOT(TakeFullScreenShot()));             // setting timer for capture delay from ini-file or defaulting to 100 ms
    }
    if (QObject::sender() == AimFrameAction) {
    }
}

void SysTrayApp::TakeFullScreenShot(void)
{
    QScreen *screen = QApplication::primaryScreen();
    QPixmap Image = screen->grabWindow(0);
    QClipboard *Clipboard = QApplication::clipboard();
    Clipboard->setPixmap(Image);
    if(QSystemTrayIcon::supportsMessages()) {
        SysTray.showMessage(SysTrayTitle1, SysTrayMessage1, QSystemTrayIcon::Information, 10000);
    }
}
