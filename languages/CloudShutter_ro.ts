<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ro_RO">
<context>
    <name>PrefWindowWidget</name>
    <message>
        <location filename="../PrefWindowWidget.ui" line="46"/>
        <source>CloudShutter Preferences</source>
        <translation>Preferințe CloudShutter</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="74"/>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="86"/>
        <source>Exit Confirmation</source>
        <translation>Confirmare ieșire</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="115"/>
        <source>Run at startup</source>
        <translation>Rulare din start</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="128"/>
        <source>Language:</source>
        <translation>Limbaj:</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="134"/>
        <source>Hot keys</source>
        <translation>Taste rapide</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="139"/>
        <source>Storage</source>
        <translation>Stocare</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="144"/>
        <source>Credits</source>
        <translation>Participare</translation>
    </message>
</context>
<context>
    <name>SysTrayApp</name>
    <message>
        <location filename="../systrayapp.cpp" line="80"/>
        <source>Ask me</source>
        <translation>Întreabă</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="76"/>
        <source>Screenshot</source>
        <translation>Capturare ecran</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="77"/>
        <source>Aim Frame</source>
        <translation>Selectare cadru</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="78"/>
        <source>Select Window</source>
        <translation>Selectare fereastră</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="79"/>
        <source>Storage options</source>
        <translation>Opțiuni stocare</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="81"/>
        <source>File</source>
        <translation>Fișier</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="82"/>
        <source>Clipboard</source>
        <translation>Clipboard</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="83"/>
        <source>Preferences</source>
        <translation>Preferințe</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="84"/>
        <source>Quit</source>
        <translation>Ieșire</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="56"/>
        <source>Do you really want to quit CloudShutter?</source>
        <translation>Sigur ieșim din CloudShutter?</translation>
    </message>
</context>
</TS>
