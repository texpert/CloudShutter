<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>PrefWindowWidget</name>
    <message>
        <location filename="../PrefWindowWidget.ui" line="46"/>
        <source>CloudShutter Preferences</source>
        <translation>CloudShutter Preferences</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="74"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="86"/>
        <source>Exit Confirmation</source>
        <translation>Exit Confirmation</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="115"/>
        <source>Run at startup</source>
        <translation>Run at startup</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="128"/>
        <source>Language:</source>
        <translation>Language:</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="134"/>
        <source>Hot keys</source>
        <translation>Hot keys</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="139"/>
        <source>Storage</source>
        <translation>Storage</translation>
    </message>
    <message>
        <location filename="../PrefWindowWidget.ui" line="144"/>
        <source>Credits</source>
        <translation>Credits</translation>
    </message>
</context>
<context>
    <name>SysTrayApp</name>
    <message>
        <location filename="../systrayapp.cpp" line="80"/>
        <source>Ask me</source>
        <translation>Ask me</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="76"/>
        <source>Screenshot</source>
        <translation>Screenshot</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="77"/>
        <source>Aim Frame</source>
        <translation>Aim Frame</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="78"/>
        <source>Select Window</source>
        <translation>Select Window</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="79"/>
        <source>Storage options</source>
        <translation>Storage options</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="81"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="82"/>
        <source>Clipboard</source>
        <translation>Clipboard</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="83"/>
        <source>Preferences</source>
        <translation>Preferences</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="84"/>
        <source>Quit</source>
        <translation>Quit</translation>
    </message>
    <message>
        <location filename="../systrayapp.cpp" line="56"/>
        <source>Do you really want to quit CloudShutter?</source>
        <translation>Do you really want to quit CloudShutter?</translation>
    </message>
</context>
</TS>
