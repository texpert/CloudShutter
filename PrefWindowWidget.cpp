#include "PrefWindowWidget.h"

PrefWindowWidget::PrefWindowWidget(QWidget *parent) :                                                       // Prefernces window' widget
    QWidget(parent),
    ui(new Ui::PrefWindowWidget)
{
    Preferences.setIniCodec("UTF-8");                                                                       //sets unicode encoding of ini-file

    ui->setupUi(this);

/*
 * Get the list of available embedded resource language files (using wildcard to get them all) and their number.
 * The ":" is the mark of embedded resources folder.
*/
    EmbeddedLanguageFiles = static_cast<QDir>(QLatin1String(":")).entryList(QStringList(QLatin1String("CloudShutter_*.qm")));
    LanguageIndex = EmbeddedLanguageFiles.size();

    ui->LanguageComboBox->blockSignals(true);                                                               //блокируем сигналы комбо-боксов на время заполнения,
    ui->DefaultPicsFormatComboBox->blockSignals(true);                                                      //чтобы не вызывались процедуры currentIndexChanged

    for (int i = 0; i < LanguageIndex; ++i) {                                                               // populating the language ComboBox
        Language = EmbeddedLanguageFiles.at(i).mid(13,2);                                                   // "en" - 2xchar language from CloudShutter_en.qm
// storing the list of available locales at the end of the list of language files for later use
        EmbeddedLanguageFiles.append(Language);
        ui->LanguageComboBox->addItem(QIcon(QLatin1String(":") + Language + QLatin1String(".svg")),         // loading the language icon & name into ComboBox
                                      QLocale::languageToString(QLocale(Language).language()));
    }

    if(Preferences.contains(QLatin1String("Language"))) {                                                   // if the INI-file has language value, get it (defaults to english if invalid)
        Language = Preferences.value(QLatin1String("Language"), QLatin1String("en")).toString();            // en
    }
    else {                                                                                                  // if no language value in ini file, get the system locale
        Language = QLocale::system().name();                                                                // en_US
        Language.truncate(Language.indexOf(QLatin1String("_")));                                            // and get from it only the first 2 chars - "en"
    }
    //QLocale::setDefault(QLocale(Language));
    LanguageIndex = EmbeddedLanguageFiles.indexOf(QLatin1String("CloudShutter_") + Language + QLatin1String(".qm"));
    if(LanguageIndex == -1) { // searching the system locale in embedded files list and getting its index
        LanguageIndex = 0;                                                                                  // if no such language embedded, returning -1 and again defaulting to english
    }
    ui->LanguageComboBox->setCurrentIndex(LanguageIndex);                                                   // setting corresponding or default language in ComboBox
    translator.load(EmbeddedLanguageFiles.at(LanguageIndex),QLatin1String(":/"));                           // loading and installing language from ini-file or default
    QApplication::installTranslator(&translator);
    ui->retranslateUi(this);


/*  Read all the CheckBoxes' states from ini-file, and set them accordingly.
 *  If no CheckBox found by the name from the file, delete this name from the ini-file.
 */
    Preferences.beginGroup(QLatin1String("CheckBoxes"));
    QStringList SavedCheckBoxes = Preferences.childKeys();
    int size = SavedCheckBoxes.size();
    for (int i = 0; i < size ; i++) {
        QCheckBox *checkBox = static_cast<QCheckBox*>(this->findChild<QCheckBox *>(SavedCheckBoxes.at(i), Qt::FindChildrenRecursively));
        if(checkBox != NULL) {
            checkBox->setCheckState(static_cast<Qt::CheckState>(Preferences.value(SavedCheckBoxes.at(i), 2).toInt()));
        }
        else {
            Preferences.remove(SavedCheckBoxes.at(i));
        }
    }

/*  Find all the check-boxes in the application and write their values to the ini-file.
 *  If any check-box was missing in the ini-file by damage or because a new one appeared in the application, its state will be saved too.
 *  Also, connect all the CheckBoxes' signal StateChanged to the universal slot on_checkBox_stateChanged
 */
    QList<QCheckBox *> AllCheckBoxes = this->findChildren<QCheckBox *>();
    size = AllCheckBoxes.size();
    for (int i = 0; i < size ; i++) {
        Preferences.setValue(AllCheckBoxes.at(i)->objectName(), AllCheckBoxes.at(i)->checkState());
        connect(AllCheckBoxes.at(i), &QCheckBox::stateChanged, this, &PrefWindowWidget::on_checkBox_stateChanged);
    }
    Preferences.endGroup();

// populating combo-box with pic formats
    ui->DefaultPicsFormatComboBox->addItems(QStringList() << QLatin1String("bmp") << QLatin1String("jpeg") << QLatin1String("jpg") << QLatin1String("png"));
    ui->DefaultPicsFormatComboBox->setCurrentText
            ((Preferences.value(QLatin1String("Default_Pics_Format"), QLatin1String("png"))).toString());       // setting format to ini-file value or default to "png"

    ui->CaptureDelaySpinBox->setValue((Preferences.value(QLatin1String("Capture_Delay"), 0.10)).toDouble());    // setting capture delay from ini-file or defaulting to 0.10 sec

//if no Pipes defined, set the default
    if(Preferences.beginReadArray(QLatin1String("Pipes")) == 0) {
        Preferences.endArray();
        Preferences.beginWriteArray(QLatin1String("Pipes"));
        Preferences.setArrayIndex(0);
        Preferences.setValue(QLatin1String("Type"), QLatin1String("Clipboard"));
        Preferences.setValue(QLatin1String("Location"), QLatin1String("System"));
        Preferences.setValue(QLatin1String("Ask"), false);
        Preferences.setValue(QLatin1String("Add"), false);
        Preferences.setArrayIndex(1);
        Preferences.setValue(QLatin1String("Type"), QLatin1String("File"));
        Preferences.setValue(QLatin1String("Location"), Preferences.fileName().section('/', 1, -2, QString::SectionIncludeLeadingSep | QString::SectionIncludeTrailingSep));
        Preferences.setValue(QLatin1String("Ask"), true);
        Preferences.setValue(QLatin1String("Add"), true);
    }
    Preferences.endArray();
    Preferences.sync();

//reading all the storage Pipes
    size = Preferences.beginReadArray(QLatin1String("Pipes"));
    ui->StorageInstancesTable->setRowCount(size);
    ui->StorageInstancesTable->horizontalHeader()->show();
    for (int i = 0; i < size ; i++) {
        Preferences.setArrayIndex(i);
        pipe Pipe;
        Pipe.Type = Preferences.value(QLatin1String("Type"), QLatin1String("File")).toString();
        ui->StorageInstancesTable->setCellWidget(i, 0, new QLabel (Pipe.Type));
        Pipe.FileName = Preferences.value(QLatin1String("Filename"), "").toString();
        Pipe.Location = Preferences.value(QLatin1String("Location"), "").toString();
        ui->StorageInstancesTable->setCellWidget(i, 1, new QLabel (Pipe.Location));
        Pipe.Login = Preferences.value(QLatin1String("Login"), "").toString();
        Pipe.Password = Preferences.value(QLatin1String("Password"), "").toString();
        Pipe.Ask = Preferences.value(QLatin1String("Ask"), true).toBool();
        Pipe.Add = Preferences.value(QLatin1String("Add"), true).toBool();
        Pipes.append(&Pipe);
    }
    Preferences.endArray();

    ui->StorageInstancesTable->resizeColumnsToContents();

//centering the PreferenceWindow
    setGeometry (QStyle::alignedRect (Qt::LeftToRight, Qt::AlignCenter, this->size(), qApp->desktop()->availableGeometry()));

    ui->LanguageComboBox->blockSignals(false);
    ui->DefaultPicsFormatComboBox->blockSignals(false);
}


PrefWindowWidget::~PrefWindowWidget()
{
    delete ui;
}


int PrefWindowWidget::QueryCheckBoxState(QString name)
{
    QCheckBox *checkBox = static_cast<QCheckBox*>(this->findChild<QCheckBox *>(name, Qt::FindChildrenRecursively));
    return checkBox->checkState();
}


void PrefWindowWidget::on_checkBox_stateChanged(int state)
{
    Preferences.setValue(QLatin1String("CheckBoxes/") + static_cast<QCheckBox*>(sender())->objectName(), state);
}


void PrefWindowWidget::on_CaptureDelaySpinBox_valueChanged(double CaptureDelay)
{
    Preferences.setValue(QLatin1String("Capture_Delay"), CaptureDelay);
}


void PrefWindowWidget::on_LanguageComboBox_currentIndexChanged(int index)
{
    translator.load(EmbeddedLanguageFiles.at(index),QLatin1String(":/"));                                         // loading and installing corresponding language
    if(translator.isEmpty() == false) {
        QCoreApplication::removeTranslator(&translator);                                            // если это не первая загрузка, выгружаем предыдущий транслятор
    }
    QCoreApplication::installTranslator(&translator);
    Preferences.setValue(QLatin1String("Language"), EmbeddedLanguageFiles.at(index + 3));                         // записываем в ini-file выбранный язык. index + 3 ибо языки записаны в том же списке, что имена встроенных языковых файлов

    LanguageChanged();                                                                              // sending signal to translate SysTrayMenu
    ui->retranslateUi(this);                                                                        // sending signal to translate Preferences Window
}


void PrefWindowWidget::on_DefaultPicsFormatComboBox_currentIndexChanged(const QString &DefaultPicsFormat)
{
    Preferences.setValue(QLatin1String("Default_Pics_Format"), DefaultPicsFormat);
}


void PrefWindowWidget::on_SecondInstanceStartTry()
{
//UPSTREAM should fix restore from minimized window https://bugreports.qt-project.org/browse/QTBUG-31117
    activateWindow();
    setWindowState(Qt::WindowActive);
    show();
}

