#include "semaphorelistener.h"

SemaphoreListener::SemaphoreListener(QSystemSemaphore *semafor, QObject *parent) :
    QObject(parent)
{
    SemaphorePtr = semafor;
}

void SemaphoreListener::StartListening ()
{
    while (true) {
        SemaphorePtr->acquire();
        emit SemaphoreAccessed();
    }
}
