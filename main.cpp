#include "systrayapp.h"
//#include <QLatin1String>

int main(int argc, char *argv[])
{
    int result = 0;
// CloudShutter running scope to get all the objets destroyed before main() ending
    {
        QApplication::setApplicationName(QLatin1String("CloudShutter"));                               //setting application & organization names for use by QSettings
        QApplication::setOrganizationName(QLatin1String("REG.RU"));
        QApplication::setOrganizationDomain(QLatin1String("reg.ru"));
        QSettings::setDefaultFormat(QSettings::IniFormat);                                  //setting ini-format of preferences file before creating QSettings object

        SysTrayApp CloudShutter(argc, argv);
        CloudShutter.setQuitOnLastWindowClosed(false);

        //qWarning() << "styleSheet = " << CloudShutter.styleSheet();

        //CloudShutter.setStyleSheet("QTableWidget::item { padding: 2px }");

        result = CloudShutter.exec();
    }   // CloudShutter running scope to get all the objets destroyed before main() ending
    return result;
}
