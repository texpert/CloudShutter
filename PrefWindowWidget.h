#ifndef PREFWINDOWWIDGET_H
#define PREFWINDOWWIDGET_H

#include <QWidget>
#include <QSettings>
#include <QStringList>
#include <QDir>
#include <QTranslator>
#include <QImageWriter>
#include <QDesktopWidget>
#include "ui_PrefWindowWidget.h"

#ifdef QT_DEBUG
    #include <QtDebug>
#endif

namespace Ui {
class PrefWindowWidget;
}

class PrefWindowWidget : public QWidget
{
    Q_OBJECT

    public:
        QSettings Preferences;

        explicit PrefWindowWidget(QWidget *parent = 0);
        ~PrefWindowWidget();

        int QueryCheckBoxState(QString name);

    public slots:
        void on_SecondInstanceStartTry();

    private:
        int LanguageIndex;
        QTranslator translator;                                                             //constructing translator before of all other objects
        QString Language;
        QStringList EmbeddedLanguageFiles;
        struct pipe {
                QString Type;
                QString FileName;
                QString Location;
                QString Login;
                QString Password;
                bool Ask;
                bool Add;
        };
        QList<pipe *> Pipes;

        Ui::PrefWindowWidget *ui;

    signals:
        void LanguageChanged();

    private slots:

/*        void on_ExitConfirmationCheckBox_stateChanged(int state);
        void on_RunStartCheckBox_stateChanged(int state);
        void on_ShowNotificationsCheckBox_stateChanged(int state);
        void on_AudioNotificationsCheckBox_stateChanged(int state);
        void on_CaptureBordersCheckBox_stateChanged(int state);*/

        //void slot_checkBox_stateChanged(int state);
        void on_checkBox_stateChanged(int state);

        void on_DefaultPicsFormatComboBox_currentIndexChanged(const QString &DefaultPicsFormat);

        void on_CaptureDelaySpinBox_valueChanged(double CaptureDelay);
        void on_LanguageComboBox_currentIndexChanged(int index);
};

#endif // WIDGET_H
