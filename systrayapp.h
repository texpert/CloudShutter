#ifndef SYSTRAYAPP_H
#define SYSTRAYAPP_H

#include "PrefWindowWidget.h"
#include "semaphorelistener.h"
#include <QMenu>
#include <QSystemTrayIcon>
#include <QMessageBox>
#include <QScreen>
#include <QClipboard>
#include <QApplication>
#include <QTimer>
#include <QSharedMemory>
#include <QTextStream>
#include <QThread>


extern QSettings Preferences;

class SysTrayApp : public QApplication{
    Q_OBJECT

    public:
        SysTrayApp(int &argv, char **args);
        ~SysTrayApp();

    private:
        PrefWindowWidget PreferenceWindow;
        QIcon SysTrayIcon;
        QMessageBox msgBox;
        QMenu SysTrayMenu, SysTrayPipesMenu;
        QString SysTrayTitle1, SysTrayMessage1, Username;
        QSystemTrayIcon SysTray;

        QThread *InstanceSemaphoreListenerThread;
        QSystemSemaphore *SingleAppMemoryAccessSemaphore, *SingleAppInstanceSemaphore;
        QSharedMemory *SingleAppFlag;
        SemaphoreListener *InstanceSemaphoreListener;

        QAction *AskAction, *ToFileAction, *ToClipboardAction, *ScreenshotAction,
            *AimFrameAction, *SelectWindowAction, *PreferencesWindowAction, *QuitAction;        // SysTray menu actions

    private slots:
        void SysTrayRetranslate();                                                              // slot for receiving LanguageChange signal and translating SysTray text entries
        void ScreenShotTimer();
        void TakeFullScreenShot();
        void quit();

    signals:
        void StartSemaphoreListener();
};


#endif // SYSTRAYAPP_H
